var pass = 50;      // Minimalna liczba punktów zaliczających test.
var score = 75;     // Bieżący wynik.
var msg;            // Komunikat.

// Przygotowanie komunikatu w oparciu o wynik uzyskany przez użytkownika.
if (score > pass) {
  msg = 'Gratulacje, zaliczyłeś test!';
} else {
  msg = 'Spróbuj ponownie!';
}

var el = document.getElementById('answer');
el.textContent = msg;