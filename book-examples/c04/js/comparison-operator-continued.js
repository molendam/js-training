var score1 = 90;     // Wynik rundy 1.
var score2 = 95;     // Wynik rundy 2.
var highScore1 = 75; // Najwyższy wynik rundy 1.
var highScore2 = 95; // Najwyższy wynik rundy 2.

// Sprawdzenie, czy łączny wynik jest większy od aktualnie najlepszych wyników.
var comparison = (score1 + score2) > (highScore1 + highScore2);

// Wyświetlenie komunikatu na stronie.
var el = document.getElementById('answer');
el.innerHTML = 'Nowy najwyższy wynik: ' + comparison;