var score1 = 8;   // Wynik rundy 1.
var score2 = 8;   // Wynik rundy 2.
var pass1 = 6;    // Wartość zaliczająca dla rundy 1.
var pass2 = 6;    // Wartość zaliczająca dla rundy 2.

// Sprawdzenie, czy użytkownik zaliczył obie rundy. Wynik zostaje umieszczony w zmiennej.
var passBoth = (score1 >= pass1) && (score2 >= pass2);

// Utworzenie komunikatu.
var msg = 'Zaliczenie obu rund: ' + passBoth;

// Wyświetlenie komunikatu na stronie.
var el = document.getElementById('answer');
el.innerHTML = msg;