var score = 75;    // Wynik.
var msg = '';      // Komunikat.

function congratulate() {
    msg += 'Gratulacje! ';
}

if (score >= 50) {  // Jeżeli wynik wynosi 50 lub więcej.
  congratulate();
  msg += 'Przechodzisz do kolejnej rundy.';
}

var el = document.getElementById('answer');
el.innerHTML = msg;