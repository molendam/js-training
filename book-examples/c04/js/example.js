var table = 3;             // Liczba, na której będą przeprowadzane operacje.
var operator = 'addition'; // Rodzaj obliczeń.
var i = 1;                 // Przypisanie licznikowi wartości 1.
var msg = '';              // Komunikat.

if (operator === 'addition') {
  // Dodawanie.
  while (i < 11) {
    msg += i + ' + ' + table + ' = ' + (i + table) + '<br />';
    i++;
  }
} else {
  // Mnożenie.
  while (i < 11) {
    msg += i + ' x ' + table + ' = ' + (i * table) + '<br />';
    i++;
  }
}

// Wyświetlenie komunikatu na stronie.
var el = document.getElementById('blackboard');
el.innerHTML = msg;