var hotItems = document.querySelectorAll('li.hot'); // Kolekcja NodeList jest przechowywana w tablicy.
if (hotItems.length > 0) {                          // Jeżeli kolekcja zawiera jakiekolwiek elementy.

  for (var i = 0; i < hotItems.length; i++) {       // Iteracja przez wszystkie elementy kolekcji.
    hotItems[i].className = 'cool';         // Zmiana wartości atrybutu class wybranego elementu.
  }

}