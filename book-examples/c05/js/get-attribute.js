var firstItem = document.getElementById('one'); // Pobranie pierwszego elementu listy.
if (firstItem.hasAttribute('class')) {          // Jeżeli ma atrybut class.
  var attr = firstItem.getAttribute('class');   // Pobranie atrybutu.

  // Umieszczenie wartości atrybutu za listą.
  var el = document.getElementById('scriptResults');
  el.innerHTML = '<p>Pierwszy element listy ma klasę o nazwie: ' + attr + '</p>';

}