var firstItem = document.getElementById('one'); // Pobranie pierwszego elementu.
firstItem.className = 'complete';               // Zmiana jego atrybutu class.

var fourthItem = document.getElementsByTagName('li').item(3);// Pobranie czwartego elementu.
// Uwaga: poniższy wiersz powinien zawierać fourthItem (a nie el2).
fourthItem.setAttribute('class', 'cool');       // Dodanie do niego atrybutu.