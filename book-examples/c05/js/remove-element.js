// Element przeznaczony do usunięcia jest przechowywany w zmiennej.
var removeEl = document.getElementsByTagName('li')[3];

// Wyszukanie jego elementu nadrzędnego.
var containerEl = document.getElementsByTagName('ul')[0];

// Usunięcie elementu.
containerEl.removeChild(removeEl);