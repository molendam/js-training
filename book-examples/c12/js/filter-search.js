(function() {                             // Umieszczenie w funkcji typu IIFE.
  var $imgs = $('#gallery img');          // Pobranie obrazów.
  var $search = $('#filter-search');      // Pobranie elementu <input>.
  var cache = [];                         // Utworzenie tablicy o nazwie cache.

  $imgs.each(function() {                 // Dla każdego obrazu.
    cache.push({                          // Dodanie obiektu do tablicy cache.
      element: this,                      // Ten obraz.
      text: this.alt.trim().toLowerCase() // Tekst jego atrybutu alt (małe litery, usunięte zbędne znaki).
    });
  });

  function filter() {                     // Zadeklarowanie funkcji filter().
    var query = this.value.trim().toLowerCase();  // Pobranie zapytania.
    cache.forEach(function(img) {         // Dla każdego elementu w tablicy cache przekazujemy obraz.
      var index = 0;                      // Przypisanie zmiennej index wartości 0.

      if (query) {                        // Jeżeli zmienna query ma wartość.
        index = img.text.indexOf(query);  // Sprawdzenie, czy tekst zmiennej query znajduje się tutaj.
      }

      img.element.style.display = index === -1 ? 'none' : '';  // Wyświetlenie lub ukrycie.
    });
  }

  if ('oninput' in $search[0]) {          // Jeżeli przeglądarka internetowa obsługuje zdarzenie input.
    $search.on('input', filter);          // Użycie zdarzenia input do wywołania funkcji filter().
  } else {                                // W przeciwnym razie.
    $search.on('keyup', filter);          // Użycie zdarzenia keyup do wywołania funkcji filter().
  }              

}());