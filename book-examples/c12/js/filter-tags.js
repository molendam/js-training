(function() {

  var $imgs = $('#gallery img');                  // Zmienna przechowuje wszystkie obrazy.
  var $buttons = $('#buttons');                   // Zmienna przechowuje przyciski.
  var tagged = {};                                // Utworzenie obiektu tagged.

  $imgs.each(function() {                         // Iteracja przez obrazy.
    var img = this;                               // Przechowywanie img w zmiennej.
    var tags = $(this).data('tags');              // Pobranie tagów danego elementu.

    if (tags) {                                   // Jeżeli element ma tagi.
      tags.split(',').forEach(function(tagName) { // Podzielenie ich w miejscu występowania przecinka.
        if (tagged[tagName] == null) {            // Jeżeli obiekt nie ma tagu,
          tagged[tagName] = [];                   // to należy dodać pustą tablicę do obiektu.
        }
        tagged[tagName].push(img);                // ADodanie obrazu do tablicy.
      });
    }
  });

  $('<button/>', {                                 // Utworzenie pustego przycisku.
    text: 'Wyświetl wszystko',                     // Dodanie tekstu "Wyświetl wszystko".
    class: 'active',                               // Przycisk staje się aktywny.
    click: function() {                            // Dodanie procedury obsługi zdarzeń click.
      $(this)                                      // Pobranie klikniętego przycisku.
        .addClass('active')                        // Dodanie klasy active.
        .siblings()                                // Pobranie pozostałych przycisków równorzędnych.
        .removeClass('active');                    // Usunięcie z nich klasy active.
      $imgs.show();                                // Wyświetlenie wszystkich obrazów.
    }
  }).appendTo($buttons);                           // Dodanie przycisku do istniejących.

  $.each(tagged, function(tagName) {               // Dla każdego znacznika.
    $('<button/>', {                               // Utworzenie pustego przycisku.
      text: tagName + ' (' + tagged[tagName].length + ')', // Dodanie nazwy tagu.
      click: function() {                          // Dodanie procedury obsługi zdarzeń click.
        $(this)                                    // Pobranie klikniętego przycisku.
          .addClass('active')                      // Dodanie klasy active.
          .siblings()                              // Pobranie pozostałych przycisków równorzędnych.
          .removeClass('active');                  // Usunięcie z nich klasy active.
        $imgs                                      // Wszystkie obrazy.
          .hide()                                  // Ukrycie ich.
          .filter(tagged[tagName])                 // Wyszukanie obrazów wraz z danym tagiem.
          .show();                                 // Wyświetlenie tylko dopasowanych obrazów.
      }
    }).appendTo($buttons);                         // Dodanie przycisku do istniejących.
  });

}());