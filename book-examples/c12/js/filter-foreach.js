$(function() {

  // Dane poszczególnych osób są przechowywane w tablicy w postaci obiektów.
  var people = [
    {                                              // Dane każdej osoby to obiekt.
      name: 'Czesław',                             // Obiekt przechowuje imię i stawkę godzinową.
      rate: 60
    },
    {
      name: 'Celina',
      rate: 80
    },
    {
      name: 'Grzegorz',
      rate: 75
    },
    {
      name: 'Nikodem',
      rate: 120
    }
  ];

  // Sprawdzenie każdej osoby oraz dodanie do tablicy tych osób, których stawka mieści się w podanym zakresie.
  var results = [];                                // Tablica osób spełniających kryteria.
  people.forEach(function(person) {                // Dla każdej osoby.
    if (person.rate >= 65 && person.rate <= 90) {  // Czy stawka mieści się w podanym zakresie?
      results.push(person);                        // Jeżeli tak, osoba zostaje dodana do tablicy.
    }
  });

  // Iteracja przez nową tablicę, dopasowane osoby są umieszczane w tabeli wyświetlanej na stronie.
  var $tableBody = $('<tbody></tbody>');           // Nowa zawartość jQuery.
  for (var i = 0; i < results.length; i++) {       // Iteracja przez dopasowane elementy.
    var person = results[i];                       // Przechowywanie bieżącej osoby.
    var $row = $('<tr></tr>');                     // Utworzenie wiersza dla osoby.
    $row.append($('<td></td>').text(person.name)); // Wstawienie imienia osoby.
    $row.append($('<td></td>').text(person.rate)); // Wstawienie stawki osoby.
    $tableBody.append( $row );                     // Dodanie wiersza do nowej zawartości.
  }

  $('thead').after($tableBody);                    // Dodanie elementu <tbody> po <thead>.
});