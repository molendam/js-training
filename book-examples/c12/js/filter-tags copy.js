(function(){

  var $imgs = $('#gallery img');                  // Zmienna przechowuje wszystkie obrazy.
  var $buttons = $('#buttons');                   // Zmienna przechowuje przyciski.
  var tagged = {};                                // Utworzenie obiektu tagged.

  $imgs.each(function(){                          // Iteracja przez obrazy.
    var img = this,                               // Przechowywanie img w zmiennej.
        tags = $(this).data('tags');              // Pobranie tagów danego elementu.

    if (tags) {                                   // Jeżeli element ma tagi.
      tags.split(',').forEach(function(tagName) { // Podzielenie ich w miejscu występowania przecinka.
        if (tagged[tagName] == null) {            // Jeżeli obiekt nie ma tagu,
          tagged[tagName] = [];                   // to należy dodać pustą tablicę do obiektu.
        }
        tagged[tagName].push(img);                // Dodanie obrazu do tablicy.
      });
    }
  });

  $('<button/>', {                                 // Utworzenie pustego przycisku.
    text: 'Wyświetl wszystko',                     // Dodanie tekstu "Wyświetl wszystko".
    class: 'active',                               // Przycisk staje się aktywny.
    click: function(e) {                           // Dodanie procedury obsługi zdarzeń click.
      $buttons.children().removeClass('active');   // Usunięcie z przycisku klasy active.
      $(this).addClass('active');                  // Dodanie klasy active.
      $imgs.show();                                // Wyświetlenie wszystkich obrazów.
    }
  }).appendTo($buttons);                           // Dodanie przycisku do istniejących.

  Object.keys(tagged).forEach(function(tagName){   // Dla każdego znacznika.
    $('<button/>', {                               // Utworzenie pustego przycisku.
      text: tagName + ' ('+tagged[tagName].length+')', // Dodanie nazwy tagu.
      click: function(e) {                         // Dodanie procedury obsługi zdarzeń click.
        $buttons.children().removeClass('active'); // Usunięcie klasy active.
        $(this).addClass('active');                // Dodanie klasy active.
        $imgs.hide();                              // Ukrycie wszystkich obrazów.
        $(tagged[tagName]).show();                 // Wyszukanie obrazów wraz z danym tagiem.
      }  
    }).appendTo($buttons);                         // Dodanie przycisku do istniejących.
  });

}());