(function() {

  // Dane poszczególnych osób są przechowywane w tablicy w postaci obiektów.
  var people = [
    {                                              // Dane każdej osoby to obiekt.
      name: 'Czesław',                             // Obiekt przechowuje imię i stawkę godzinową.
      rate: 60
    },
    {
      name: 'Celina',
      rate: 80
    },
    {
      name: 'Grzegorz',
      rate: 75
    },
    {
      name: 'Nikodem',
      rate: 120
    }
  ];

  var rows = [],                        // Tablica rows.
      $min = $('#value-min'),           // Pole tekstowe dla wartości minimalnej stawki.
      $max = $('#value-max'),           // Pole tekstowe dla wartości maksymalnej stawki.
      $table = $('#rates');             // Tabela przeznaczona na dopasowane osoby.

  function makeRows() {                 // Utworzenie tablicy i wierszy tabeli.
    people.forEach(function(person) {   // Iteracja przez wszystkie obiekty person w tablicy people.
      var $row = $('<tr></tr>');        // Utworzenie wiersza dla danej osoby.
      $row.append( $('<td></td>').text(person.name) ); // Wstawienie imienia osoby.
      $row.append( $('<td></td>').text(person.rate) ); // Wstawienie imienia osoby.
      rows.push({ // Dodanie obiektu zapewniającego odwoływanie się między tablicami people i rows.
        person: person,                 // Odwołanie do obiektu person.
        $element: $row                  // Odwołanie do wiersza jako obiektu jQuery.
      });
    });
  }

  function appendRows() {               // Dodanie wierszy do tabeli.
    var $tbody = $('<tbody></tbody>');  // Utworzenie elementu <tbody>.
    rows.forEach(function(row) {        // Dla każdego obiektu w tablicy rows.
      $tbody.append(row.$element);      // Dodanie kodu HTML dla wiersza.
    });
    $table.append($tbody);              // Dodanie wierszy do tabeli.
  }

  function update(min, max) {           // Uaktualnienie zawartości tabeli.
    rows.forEach(function(row) {        // Dla każdego wiersza w tablicy rows.
      if (row.person.rate >= min && row.person.rate <= max) { // Jeżeli wartość jest w zakresie.
        row.$element.show();            // Wyświetlenie wiersza.
      } else {                          // W przeciwnym razie.
        row.$element.hide();            // Ukrycie wiersza.
      }
    });
  }

  function init() {                     // Zadania wykonywane po pierwszym uruchomieniu skryptu.
    $('#slider').noUiSlider({           // Konfiguracja kontrolki suwaka.
      range: [0, 150], start: [65, 90], handles: 2, margin: 20, connect: true,
      serialization: {to: [$min, $max],resolution: 1}
    }).change(function() { update($min.val(), $max.val()); });
    makeRows();                         // Utworzenie tablicy rows i wierszy tabeli.
    appendRows();                       // Dodanie wierszy do tabeli.
    update($min.val(), $max.val());     // Uaktualnienie tabeli, aby zawierała dopasowane osoby.
  }

  $(init);                              // Wywołanie funkcji init() po wczytaniu modelu DOM.
}());