$(function() {

  // Dane poszczególnych osób są przechowywane w tablicy w postaci obiektów.
  var people = [
    {                                              // Dane każdej osoby to obiekt.
      name: 'Czesław',                             // Obiekt przechowuje imię i stawkę godzinową.
      rate: 60
    },
    {
      name: 'Celina',
      rate: 80
    },
    {
      name: 'Grzegorz',
      rate: 75
    },
    {
      name: 'Nikodem',
      rate: 120
    }
  ];


  // Funkcja działa w charakterze filtru.
  function priceRange(person) {                        // Zadeklarowanie funkcji priceRange().
    return (person.rate >= 65) && (person.rate <= 90); // Zwróć wartość true dla wskazanego zakresu.
  };

  // Filtrowanie tablicy people, dopasowane osoby są umieszczone w tablicy results.
  var results = [];                              // Tablica zawierająca dopasowane osoby.
  results = people.filter(priceRange);           // Funkcja filter() wywołuje priceRange().


  // Iteracja przez nową tablicę, dopasowane osoby są umieszczane w tabeli wyświetlanej na stronie.
  var $tableBody = $('<tbody></tbody>');           // Nowa zawartość jQuery.
  for (var i = 0; i < results.length; i++) {       // Iteracja przez dopasowane elementy.
    var person = results[i];                       // Przechowywanie bieżącej osoby.
    var $row = $('<tr></tr>');                     // Utworzenie wiersza dla osoby.
    $row.append($('<td></td>').text(person.name)); // Wstawienie imienia osoby.
    $row.append($('<td></td>').text(person.rate)); // Wstawienie stawki osoby.
    $tableBody.append( $row );                     // Dodanie wiersza do nowej zawartości.
  }

  $('thead').after($tableBody);                    // Dodanie elementu <tbody> po <thead>.
});