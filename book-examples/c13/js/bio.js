(function () {
  var bio = $('#bio');
  var bioCounter = $('#bio-count');

  // Po przejściu do elementu <textarea> wyświetlamy licznik i odpowiednio uaktualniamy
  // klasy w zależności od liczby pozostałych znaków.
  bio.on('focus', updateCounter);
  bio.on('keyup', updateCounter);

  // Po opuszczeniu elementu <textarea> ukrywamy licznik,
  // chyba że wprowadzono zbyt wiele znaków.
  bio.on('blur', function () {
    if (bioCounter.text() >= 0) {
      bioCounter.addClass('hide');
    }
  });


  function updateCounter(e) {
    var count = 140 - bio.val().length;
    var status = '';
    if (count < 0) {
      status = 'error';
    } else if (count <= 15) {
      status = 'warn';
    } else {
      status = 'good';
    }

    // Usunięcie poprzednich klas.
    bioCounter.removeClass('error warn good hide');
    // Dodanie nowej klasy.
    bioCounter.addClass(status);
    bioCounter.text(count);
  }

}());
