(function(){
  var form, options, other, otherText, hide;           // Deklaracja zmiennych.
  form      = document.getElementById('how-heard');    // Pobranie formularza.
  options   = form.elements.heard;                     // Pobranie przycisków opcji.
  other     = document.getElementById('other');        // Przycisk Inne.
  otherText = document.getElementById('other-text');   // Pole tekstowe pod przyciskiem Inne.
  otherText.className = 'hide';                        // Ukrycie pola tekstowego.

  for (var i = [0]; i < options.length; i++) {         // Iteracja przez przyciski opcji.
    addEvent(options[i], 'click', radioChanged);       // Dodanie obserwatora zdarzeń.
  }

  function radioChanged() {
    hide = other.checked ? '' : 'hide';                // Czy zaznaczony jest przycisk Inne?
    otherText.className = hide;                        // Widoczność pola tekstowego.
    if (hide) {                                        // Jeżeli pole tekstowe jest ukryte,
      otherText.value = '';                            // należy usunąć jego zawartość.
    }
  }
}());