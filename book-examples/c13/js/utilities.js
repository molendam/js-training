// Funkcja pomocnicza odpowiedzialna za dodanie obserwatora zdarzeń.
function addEvent (el, event, callback) {
  if ('addEventListener' in el) {                  // Jeżeli metoda addEventListener() działa,
    el.addEventListener(event, callback, false);   // należy jej użyć.
  } else {                                         // W przeciwnym razie.
    el['e' + event + callback] = callback;         // Wywołanie zwrotne metody el.
    el[event + callback] = function () {
      el['e' + event + callback](window.event);
    };
    el.attachEvent('on' + event, el[event + callback]);
  }
}

// Helper function to remove an event listener
function removeEvent(el, event, callback) {
  if ('removeEventListener' in el) {                      // Jeżeli metoda removeEventListener() działa,
    el.removeEventListener(event, callback, false);       // należy jej użyć.
  } else {                                                // W przeciwnym razie.
    el.detachEvent('on' + event, el[event + callback]);   // Utworzenie rozwiązania awaryjnego dla IE.
    el[event + callback] = null;
    el['e' + event + callback] = null;
  }
}

