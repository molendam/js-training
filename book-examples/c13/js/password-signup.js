(function () {
  var password = document.getElementById('password'); // Zmienna przechowuje hasło.
  var passwordConfirm = document.getElementById('conf-password');
 
  function setErrorHighlighter(e) {
    var target = e.target || e.srcElement;             // Pobranie elementu docelowego.
    if (target.value.length < 8) {                     // Jeżeli wartość elementu jest mniejsza niż 8 znaków.
      target.className = 'fail';                       // Przypisanie klasy fail.
    } else {                                           // W przeciwnym razie.
      target.className = 'pass';                       // Przypisanie klasy pass.
    }
  }

  function removeErrorHighlighter(e) {
    var target = e.target || e.srcElement;              // Pobranie elementu docelowego.
    if (target.className === 'fail') {                  // Jeżeli klasa to fail.
      target.className = '';                            // Usunięcie klasy.
    }
  }

  function passwordsMatch(e) {
    var target = e.target || e.srcElement;               // Pobranie elementu docelowego.
    // Jeżeli wartości hasła i potwierdzenia hasła są identyczne i mają co najmniej 8 znaków.
    if ((password.value === target.value) && target.value.length >= 8) { 
      target.className = 'pass';                         // Przypisanie klasy pass.
    } else {                                             // W przeciwnym razie.
      target.className = 'fail';                         // Przypisanie klasy fail.
    }
  }

  addEvent(password, 'focus', removeErrorHighlighter); 
  addEvent(password, 'blur', setErrorHighlighter);
  addEvent(passwordConfirm, 'focus', removeErrorHighlighter);
  addEvent(passwordConfirm, 'blur', passwordsMatch);
}());