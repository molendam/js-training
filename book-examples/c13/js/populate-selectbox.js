(function(){
  var type  = document.getElementById('equipmentType');// Rodzaj listy rozwijanej.
  var model = document.getElementById('model');        // Lista rozwijana do wyboru modelu.
  var cameras = {                                      // Obiekt przechowujący dostępne aparaty fotograficzne.
    bolex: 'Bolex Paillard H8',
    yashica: 'Yashica 30',
    pathescape: 'Pathescape Super-8 Relax',
    canon: 'Canon 512'
  };
  var projectors = {                                  // Obiekt przechowujący dostępne projektory.
    kodak: 'Kodak Instamatic M55',
    bolex: 'Bolex Sound 715',
    eumig: 'Eumig Mark S',
    sankyo: 'Sankyo Dualux'
  };

  // Kiedy użytkownik zmieni wartość pierwszej listy rozwijanej.
  addEvent(type, 'change', function() {
    if (this.value === 'choose') {                // Nie dokonano wyboru.
      model.innerHTML = '<option>Proszę najpierw wybrać rodzaj sprzętu</option>';
      return;                                     // Nie ma potrzeby dalszego przetwarzania.
    }
    var models = getModels(this.value);           // Wybór odpowiedniego obiektu.

    // Iteracja przez opcje w obiekcie w celu utworzenia opcji dla listy.
    var options = '<option>Proszę wybrać model</option>';
var key;
    for (key in models) {                     // Iteracja przez modele.
      options += '<option value="' + key + '">' + models[key] + '</option>';
    } // Jeżeli opcja może zawierać znak cytowania, klucz powinien być unieszkodliwiony.
    model.innerHTML = options;                    // Uaktualnienie drugiej listy rozwijanej.
  });

  function getModels(equipmentType) {
    if (equipmentType === 'cameras') {            // Jeżeli wybrano aparat fotograficzny,
      return cameras;                             // należy zwrócić obiekt cameras.
    } else if (equipmentType === 'projectors') {  // Jeżeli wybrano projektory,
      return projectors;                          // należy zwrócić obiekt projectors.
    }
  }
}());