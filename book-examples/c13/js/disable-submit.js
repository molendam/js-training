(function(){
  var form      = document.getElementById('newPwd');  // Formularz.
  var password  = document.getElementById('pwd');     // Pole hasła.
  var submit    = document.getElementById('submit');  // Przycisk wysyłający formularz.
  
  var submitted = false;                            // Czy formularz został wysłany?
  submit.disabled = true;                           // Wyłączenie przycisku wysyłającego formularz.
  submit.className = 'disabled';                    // Nadanie stylu przyciskowi wysyłającemu formularz.

  // Zdarzenie input: sprawdzenie, czy należy włączyć przycisk wysyłający formularz.
  addEvent(password, 'input', function(e) {         // Po wystąpieniu zdarzenia input w polu hasła.
    var target = e.target || e.srcElement;          // Element docelowy zdarzenia.
    submit.disabled = submitted || !target.value;   // Ustawienie właściwości disabled.
    // Jeżeli formularz został wysłany lub pole hasła jest puste, należy ustawić wartość CSS disabled.
    submit.className = (submitted || !target.value) ? 'disabled' : 'enabled';
  }); 

  // Zdarzenie submit: wyłączenie formularza, aby nie mógł być ponownie wysłany.
  addEvent(form, 'submit', function(e) {            // Po wystąpieniu zdarzenia submit.
    if (submit.disabled || submitted) {             // Jeżeli przycisk jest wyłączony LUB formularz został wysłany.
      e.preventDefault();                           // Zatrzymanie wysyłania formularza.
      return;                                       // Zatrzymanie przetwarzania funkcji.
    }                                               // W przeciwnym razie należy kontynuować...
    submit.disabled = true;                         // Wyłączenie przycisku wysyłającego formularz.
    submitted = true;                               // Uaktualnienie zmiennej submitted.
    submit.className = 'disabled';                  // Uaktualnienie stylu.

    // Tylko w celach demonstracyjnych: zobacz, co zostało wysłane, i przekonaj się, że przycisk wysyłający formularz jest wyłączony.
    e.preventDefault();                             // Uniemożliwienie wysłania formularza.
    alert('Hasło to ' + password.value);            // Wyświetlenie komunikatu.
  });
}());