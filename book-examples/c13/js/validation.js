// Kod JavaScript odpowiedzialny za weryfikację formularza.
// A. Funkcja anonimowa wywoływana przez zdarzenie submit.
// B. Funkcje wywoływane w celu przeprowadzenia sprawdzenia podstawowego za pomocą funkcji anonimowej w sekcji A.
// C. Funkcje wywoływane w celu przeprowadzenia sprawdzenia podstawowego za pomocą funkcji anonimowej w sekcji A.
// D. Funkcje odpowiedzialne za pobieranie, konfigurację, wyświetlanie i usuwanie komunikatów błędów.
// E. Obiekt sprawdzający typ danych za pomocą wyrażeń regularnych i funkcji validateTypes() zdefiniowanej w sekcji B.

// Zależności: jQuery, jQueryUI, birthday.js, styles.css

(function () {
  document.forms.register.noValidate = true; // Wyłączenie weryfikacji wbudowanej w HTML5, zamiast niej używamy JavaScript.
  // -------------------------------------------------------------------------
  //  A) Funkcja anonimowa wywoływana przez zdarzenie submit.
  // -------------------------------------------------------------------------
  $('form').on('submit', function (e) {      // Kiedy formularz zostanie wysłany.
    var elements = this.elements;            // Kolekcja kontrolek formularza.
    var valid = {};                          // Własny obiekt weryfikacji.
    var isValid;                             // isValid: sprawdza kontrolki formularza.
    var isFormValid;                         // isFormValid: sprawdza cały formularz.

    // Przeprowadzenie sprawdzenia podstawowego (wywołanie funkcji poza procedurą obsługi zdarzeń).
    var i;
    for (i = 0, l = elements.length; i < l; i++) {
      // W kolejnym wierszu mamy wywołanie funkcji validateRequired()  i validateTypes().
      isValid = validateRequired(elements[i]) && validateTypes(elements[i]); 
      if (!isValid) {                    // Jeżeli element nie zalicza tych dwóch testów.
        showErrorMessage(elements[i]);   // Wyświetlenie komunikatów o błędach.
      } else {                           // W przeciwnym razie.
        removeErrorMessage(elements[i]); // Usunięcie komunikatów o błędach.
      }                                  // Koniec polecenia if.
      valid[elements[i].id] = isValid;   // Dodanie elementu do obiektu valid.
    }                                    // Koniec pętli for.

    // Przeprowadzenie własnej weryfikacji
    // (dane wejściowe bio można buforować w zmiennej).
    if (!validateBio()) {                // Wywołanie validateBio(), jeżeli kontrolka jest nieprawidłowa.
      showErrorMessage(document.getElementById('bio')); // Wyświetlenie komunikatu o błędzie.
      valid.bio = false;                 // Uaktualnienie obiektu - element niepoprawny.
    } else {                             // W przeciwnym razie usuń komunikat błędu.
      removeErrorMessage(document.getElementById('bio'));
    }

    // Hasło (można je buforować w zmiennej).
    if (!validatePassword()) {          // Wywołanie validatePassword(), jeżeli kontrolka jest nieprawidłowa.
      showErrorMessage(document.getElementById('password')); // Wyświetlenie komunikatu o błędzie.
      valid.password = false;           // Uaktualnienie obiektu - element niepoprawny.
    } else {                            // W przeciwnym razie usuń komunikat błędu.
      removeErrorMessage(document.getElementById('password'));
    }

    // Zgoda rodziców (te informacje można buforować w zmiennej).
    if (!validateParentsConsent()) {     // Wywołanie validateParentalConsent(), jeżeli kontrolka jest nieprawidłowa.
      showErrorMessage(document.getElementById('parents-consent')); // Wyświetlenie komunikatu o błędzie.
      valid.parentsConsent = false;      // Uaktualnienie obiektu - element niepoprawny.
    } else {                             // W przeciwnym razie usuń komunikat błędu.
      removeErrorMessage(document.getElementById('parents-consent'));
    }

    // Czy testy zostały zaliczone i czy można wysłać formularz?
    // Iteracja przez obiekt valid; jeżeli wystąpiły błędy, to zmienna isFormValid ma przypisaną wartość false.
    for (var field in valid) {          // Sprawdzenie właściwości obiektu valid.
      if (!valid[field]) {              // Jeżeli element jest nieprawidłowy.
        isFormValid = false;            // Przypisanie zmiennej isFormValid wartości false.
        break;                          // Zatrzymanie pętli for, znaleziono błąd.
      }                                 // W przeciwnym razie.
      isFormValid = true;               // Formularz jest wypełniony prawidłowo, można go wysłać.
    }


    // Jeżeli formularz jest wypełniony nieprawidłowo, nie wolno go wysłać.
    if (!isFormValid) {                 // Jeżeli zmienna isFormValid ma wartość inną niż true.
      e.preventDefault();               // Uniemożliwienie wysłania formularza.
    }

  });                                   // Koniec procedury obsługi zdarzeń.
  //  Koniec funkcji anonimowej wywołanej przez naciśnięcie przycisku wysyłającego formularz.


  // -------------------------------------------------------------------------
  // B) Funkcje odpowiedzialne za przeprowadzenie sprawdzenia podstawowego.
  // -------------------------------------------------------------------------

  // Sprawdzenie, czy pole jest wymagane. Jeżeli tak, sprawdzamy czy posiada wartość.
  // Do działania wymaga przedstawionych poniżej isRequired() i isEmpty(), a także zdefiniowanej dalej funkcji setErrorMessage().
  function validateRequired(el) {
    if (isRequired(el)) {                           // Czy element jest wymagany.
      var valid = !isEmpty(el);                     // Czy element posiada wartość (true/false).
      if (!valid) {                                 // Jeżeli wartością zmiennej valid jest false.
        setErrorMessage(el,  'To pole jest wymagane');  // Przygotowanie komunikatu o błędzie.
      }
      return valid;                                 // Zwrot zmiennej valid (true/false)
    }
    return true;                                    // Jeżeli nie jest wymagany, to wszystko w porządku.
  }

  // Sprawdzenie, czy element jest wymagany.
  // Ta funkcja jest wywoływana przez validateRequired().
  function isRequired(el) {
   return ((typeof el.required === 'boolean') && el.required) ||
     (typeof el.required === 'string');
  }

  // Sprawdzenie, czy element jest pusty (lub czy jego wartość jest taka sama, jak atrybutu placeholder).
  // Przeglądarki HTML5 pozwalają użytkownikowi na podanie takiego samego tekstu, jak tekst podpowiedzi. Jednak w omawianym przypadku taka możliwość jest niedozwolona.
  // Ta funkcja jest wywoływana przez validateRequired().
  function isEmpty(el) {
    return !el.value || el.value === el.placeholder;
  }

  // Sprawdzenie, czy wartość pasuje do typu atrybutu.
  // Działanie funkcji opiera się na obiekcie validateType (przedstawiony na końcy funkcji typu IIFE).
  function validateTypes(el) {
    if (!el.value) return true;                     // Jeżeli element nie ma wartości, należy zwrócić true.
                                                    // W przeciwnym razie trzeba pobrać wartość za pomocą metody .data()
    var type = $(el).data('type') || el.getAttribute('type');  // lub pobrać atrybut type pola tekstowego.
    if (typeof validateType[type] === 'function') { // Czy atrybut wskazuje metodę do weryfikacji obiektu?
      return validateType[type](el);                // Jeżeli tak, sprawdzamy poprawność wartości.
    } else {                                        // Jeżeli nie.
      return true;                                  // Zwracamy true, ponieważ wartości nie można przetestować.
    }
  }

  // -------------------------------------------------------------------------
  // C) Funkcje odpowiedzialne za przeprowadzenie niestandardowej weryfikacji.
  // -------------------------------------------------------------------------

  // Jeżeli użytkownik ma mniej niż 13 lat, należy sprawdzić, czy otrzymał zgodę rodziców.
  // Zależność: birthday.js (w przeciwnym razie operacja sprawdzenia się nie powiedzie).
  function validateParentsConsent() {
    var parentsConsent   = document.getElementById('parents-consent');
    var consentContainer = document.getElementById('consent-container');
    var valid = true;                          // Zmienna: valid otrzymuje wartość true.
    if (consentContainer.className.indexOf('hide') === -1) { // Jeżeli pole wyboru jest wyświetlone.
      valid = parentsConsent.checked;          // Uaktualnienie zmiennej valid: jest zaznaczona lub nie.
      if (!valid) {                            // Jeżeli nie, należy wyświetlić komunikat o błędzie.
        setErrorMessage(parentsConsent, 'Wymagana jest zgoda rodziców');
      }
    }
    return valid;                               // Zwrócenie informacji o prawidłowości elementu.
  }

  // Sprawdzenie, czy informacje o użytkowniku mają 140 znaków lub mniej.
  function validateBio() {
    var bio = document.getElementById('bio');
    var valid = bio.value.length <= 140;
    if (!valid) {
      setErrorMessage(bio, 'Informacje o Tobie mogą mieć maksymalnie 140 znaków');
    }
    return valid;
  }

  // Sprawdzenie, czy hasło ma przynajmniej 8 znaków.
  function validatePassword() {
    var password = document.getElementById('password');
    var valid = password.value.length >= 8;
    if (!valid) {
      setErrorMessage(password, 'Hasło musi składać się z przynajmniej 8 znaków');
    }
    return valid;
  }



  // -------------------------------------------------------------------------
  // D) Funkcje odpowiedzialne za pobieranie, konfigurację, wyświetlanie i usuwanie komunikatów błędów.
  // -------------------------------------------------------------------------

  function setErrorMessage(el, message) {
    $(el).data('errorMessage', message);                 // Przechowywanie komunikatu o błędzie wraz z elementem.
  }

  function getErrorMessage(el) {
    return $(el).data('errorMessage') || el.title;       // Pobranie komunikatu błędu lub tytułu elementu.
  }

  function showErrorMessage(el) {
    var $el = $(el);                                     // Wyszukanie elementu z błędem.
    var errorContainer = $el.siblings('.error.message'); // Czy zawiera już jakiekolwiek błędy?

    if (!errorContainer.length) {                         // Jeżeli nie znaleziono komunikatów o błędach.
       // Utworzenie elementu <span> do przechowywania błędu i dodanie go po elemencie, w którym wystąpił błąd.
       errorContainer = $('<span class="error message"></span>').insertAfter($el);
    }
    errorContainer.text(getErrorMessage(el));             // Dodanie komunikatu o błędzie.
  }

  function removeErrorMessage(el) {
    var errorContainer = $(el).siblings('.error.message'); // Pobranie elementów równorzędnych tej kontrolki formularza używanej do przechowywania komunikatu błędu.
    errorContainer.remove();                               // Usunięcie elementu zawierającego komunikat błędu.
  }



  // -------------------------------------------------------------------------
  // E) Obiekt sprawdzający typ danych.
  // -------------------------------------------------------------------------

  // Sprawdzenie, czy dane są prawidłowe. Jeżeli nie, następuje przygotowanie komunikatu błędu.
  // Wartością zwrotną jest true dla prawidłowych danych lub false w przypadku nieprawidłowych.
  var validateType = {
    email: function (el) {                                 // Utworzenie metody email().
      // Proste wyrażenie regularne, które sprawdza wystąpienie pojedynczego znaku @ w adresie e-mail.
      var valid = /[^@]+@[^@]+/.test(el.value);            // Wynik testu jest przechowywany w zmiennej valid.
      if (!valid) {                                        // Jeżeli wartością zmiennej valid nie jest true.
        setErrorMessage(el, 'Proszę podać poprawny adres e-mail'); // Przygotowanie komunikatu o błędzie.
      }
      return valid;                                        // Zwrot zmiennej valid.
    },
    number: function (el) {                                // Utworzenie metody number().
      var valid = /^\d+$/.test(el.value);                  // Wynik testu jest przechowywany w zmiennej valid.
      if (!valid) {
        setErrorMessage(el, 'Proszę podać poprawną liczbę');
      }
      return valid;
    },
    date: function (el) {                                  // Utworzenie metody date().
                                                           // Wynik testu jest przechowywany w zmiennej valid.
      var valid = /^(\d{2}\/\d{2}\/\d{4})|(\d{4}-\d{2}-\d{2})$/.test(el.value);
      if (!valid) {                                        // Jeżeli wartością zmiennej valid nie jest true.
        setErrorMessage(el, 'Proszę podać poprawną datę'); // Przygotowanie komunikatu o błędzie.
      }
      return valid;                                        // Zwrot zmiennej valid.
    }
  };

}());  // Koniec funkcji typu IIFE.