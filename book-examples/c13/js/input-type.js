(function(){

  var pwd = document.getElementById('pwd');     // Pobranie pola hasła.
  var chk = document.getElementById('showPwd'); // Pobranie pola wyboru.

  addEvent(chk, 'change', function(e) {         // Gdy użytkownik kliknie pole wyboru.
    var target = e.target || e.srcElement;      // Pobranie tego elementu.
    try {                                       // Próba wykonania poniższego fragmentu kodu.
      if (target.checked) {                     // Jeżeli pole wyboru jest zaznaczone.
        pwd.type = 'text';                      // Przypisanie atrybutowi type wartości text.
      } else {                                  // W przeciwnym razie.
        pwd.type = 'password';                  // Przypisanie atrybutowi type wartości password.
      }
    } catch(error) {                            // Jeżeli zmiana spowoduje wystąpienie błędu.
      alert('Przeglądarka nie może zmienić rodzaju pola'); // Jeżeli zmiana spowoduje wystąpienie błędu.
    }
  });

}());