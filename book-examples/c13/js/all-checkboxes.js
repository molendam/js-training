(function(){
  var form     = document.getElementById('interests'); // Pobranie formularza.
  var elements = form.elements;                      // Wszystkie elementy w formularzu.
  var options  = elements.genre;                     // Tablica: pola wyboru zainteresowań.
  var all      = document.getElementById('all');     // Pole wyboru 'Wszystkie'.

  function updateAll() {
    for (var i = 0; i < options.length; i++) {       // Iteracja przez pola wyboru.
      options[i].checked = all.checked;              // Uaktualnienie właściwości checked.
      console.log(options[i].name);
    }
  }
  addEvent(all, 'change', updateAll);                // Dodanie obserwatora zdarzeń.

  function clearAllOption(e) {
    var target = e.target || e.srcElement;           // Pobranie elementu docelowego dla zdarzenia.
    if (!target.checked) {                           // Jeżeli element nie jest zaznaczony.
      all.checked = false;                           // Usunięcie zaznaczenia opcji 'Wszystkie'.
    }
  }
  for (var i = 0; i < options.length; i++) {         // Iteracja przez pola wyboru.
    addEvent(options[i], 'change', clearAllOption);  // Dodanie obserwatora zdarzeń.
  }

}());