(function() {                                        // Umieszczenie kodu w funkcji typu IIFE.
  // Test: utworzenie elementu <input> i sprawdzenie, czy atrybut placeholder jest obsługiwany.
  if ('placeholder' in document.createElement('input')) {
    return;
  }

  var length = document.forms.length;               // Określenie liczby formularzy na stronie.
  for (var i = 0, l = length; i < l; i++ ) {        // Iteracja przez wszystkie formularze.
    showPlaceholder(document.forms[i].elements);    // Wywołanie funkcji showPlaceholder().
  }

  function showPlaceholder(elements) {              // Deklaracja funkcji.
    for (var i = 0, l = elements.length; i < l; i++) { // Dla każdego elementu.
      var el = elements[i];                         // Przechowywanie tego elementu.

      if (!el.placeholder) {                        // Jeżeli nie został zdefiniowany atrybut placeholder.
        continue;                                   // Przejście do następnego elementu.
      }                                             // W przeciwnym razie.

      el.style.color = '#666666';                   // Ustawienie koloru tekstu na szary.
      el.value = el.placeholder;                    // Dodanie tekstu podpowiedzi.

      addEvent(el, 'focus', function () {           // Jeżeli element stanie się aktywny.
        if (this.value === this.placeholder) {      // Jeżeli wartość=wartość atrybutu placeholder.
          this.value = '';                          // Usunięcie zawartości pola tekstowego.
          this.style.color = '#000000';             // Przywrócenie domyślnego koloru tekstu.
        }
      });

      addEvent(el, 'blur', function () {            // W przypadku zdarzenia blur.
        if (this.value === '') {                    // Jeżeli pole tekstowe jest puste.
          this.value = this.placeholder;            // Wartość staje się podpowiedzią.
          this.style.color = '#666666';             // Ustawienie koloru szarego dla tekstu.
        }
      }); 
    }                                                // Koniec pętli for.
  }                                                  // Koniec funkcji showPlaceholder().
}());