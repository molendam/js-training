(function(){ 
  var form = document.getElementById('login');       // Pobranie elementu <form>.

  addEvent(form, 'submit', function(e) {             // Kiedy użytkownik wyśle formularz.
    e.preventDefault();                              // Uniemożliwienie wysłania formularza.
    var elements = this.elements;                    // Pobranie wszystkich elementów formularza.
    var username = elements.username.value;          // Pobranie wprowadzonej nazwy użytkownika.
    var msg      = 'Witaj, ' + username;             // Utworzenie komunikatu z powitaniem.
    document.getElementById('main').textContent = msg; // Wyświetlenie komunikatu z powitaniem.
  });
}());