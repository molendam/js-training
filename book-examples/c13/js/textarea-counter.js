(function () {
  var bio      = document.getElementById('bio'),        // Element <textarea>.
      bioCount = document.getElementById('bio-count');  // Element licznika znaków.

  addEvent(bio, 'focus', updateCounter);       // Wywołanie updateCounter()po wystąpieniu zdarzenia focus.
  addEvent(bio, 'input', updateCounter);       // Wywołanie updateCounter()po wystąpieniu zdarzenia input.

  addEvent(bio, 'blur', function () {          // Po opuszczeniu elementu.
    if (bio.value.length <= 140) {             // Jeżeli wprowadzone informacje nie są za długie.
      bioCount.className = 'hide';             // Ukrycie licznika.
    }
  });

  function updateCounter(e) {
    var target = e.target || e.srcElement;      // Pobranie celu zdarzenia.
    var count = 140 - target.value.length;      // Liczba znaków pozostałych do użycia.
    if (count < 0) {                            // Jeżeli pozostało mniej niż 0 znaków,
      bioCount.className = 'error';             // wtedy należy dodać klasę error.
    } else if (count <= 15) {                   // Jeżeli pozostało mniej niż 15 znaków,
      bioCount.className = 'warn';              // należy dodać klasę warn.
    } else {                                    // W przeciwnym razie
      bioCount.className = 'good';              // należy dodać klasę good.
    }
    var charMsg = '<b>' + count + '</b>' + ' znaki'; // Komunikat do wyświetlenia.
    bioCount.innerHTML = charMsg;               // Uaktualnienie elementu licznika.
  }

}());