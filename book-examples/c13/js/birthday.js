(function () {
  var $birth = $('#birthday');                         // Podana data urodzenia.
  var $parentsConsent = $('#parents-consent');         // Pole wyboru oznaczające zgodę rodziców.
  var $consentContainer = $('#consent-container');     // Kontener dla pola wyboru.

  // Utworzenie za pomocą jQuery UI kontrolki do wyboru daty.
  $birth.prop('type', 'text').data('type', 'date').datepicker({
    dateFormat: 'yy-mm-dd'
  });

  $birth.on('blur change', checkDate);                 // Pole wprowadzenia daty staje się nieaktywne.

  function checkDate() {                               // Zadeklarowanie checkDate().
    var dob = this.value.split('-');                   // Tablica na podstawie daty.
    // Przekazanie funkcji toggleParentsConsent() daty urodzenia użytkownika w postaci obiektu Date.
    toggleParentsConsent(new Date(dob[0], dob[1] - 1, dob[2]));
  }

  function toggleParentsConsent(date) {                 // Deklaracja funkcji.
    if (isNaN(date)) return;                            // Zatrzymanie działania, jeśli data jest nieprawidłowa.
    var now = new Date();                               // Nowy obiekt daty: dzisiejsza.
    // Jeżeli różnica (teraz minus data urodzenia wynosi mniej niż 13 lat),
    // to należy wyświetlić pole wyboru oznaczające zgodę rodziców. (Nie uwzględniamy istnienia lat przestępnych).
    // Aby uzyskać 13 lat, trzeba użyć wartości: milisekundy * sekundy * minuty * godziny * dni * lata.
    if ((now - date) < (1000 * 60 * 60 * 24 * 365 * 13)) { 
      $consentContainer.removeClass('hide');            // Usunięcie klasy hide.
      $parentsConsent.focus();                          // Pole staje się aktywne.
    } else {                                            // W przeciwnym razie.
      $consentContainer.addClass('hide');               // Dodanie klasy hide.
      $parentsConsent.prop('checked', false);           // Przypisanie właściwości checked wartości false.
    }
  }
}());