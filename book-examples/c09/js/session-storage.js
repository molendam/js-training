if (Modernizr.sessionstorage) {

  var txtUsername = document.getElementById('username'),  // Pobranie elementów formularza.
      txtAnswer = document.getElementById('answer');

  txtUsername.value = sessionStorage.getItem('username'); // Wartości elementów są
  txtAnswer.value = sessionStorage.getItem('answer');     // pobrane z obiektu sessionStorage.

  txtUsername.addEventListener('input', function () {     // Dane zostały zapisane.
    sessionStorage.setItem('username', txtUsername.value);
  }, false);

  txtAnswer.addEventListener('input', function () {       // Dane zostały zapisane.
    sessionStorage.setItem('answer', txtAnswer.value);
  }, false);
}