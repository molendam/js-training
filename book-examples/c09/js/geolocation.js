var elMap = document.getElementById('loc');                 // Element HTML.
var msg = 'Przepraszamy, nie udało się ustalić Twojego położenia.';    // Komunikat o braku danych dotyczących położenia.

if (Modernizr.geolocation) {                                // Czy geolokalizacja jest obsługiwana?
  navigator.geolocation.getCurrentPosition(success, fail);  // Pytanie o zgodę na użycie informacji.
  elMap.textContent = 'Sprawdzanie położenia...';           // Komunikat informujący o sprawdzeniu położenia....
} else {                                                    // Brak obsługi geolokalizacji.
  elMap.textContent = msg;                                  // Wyświetlenie komunikatu.
}

function success(location) {                                // Otrzymano dane o położeniu użytkownika.
  msg = '<h3>Długość geograficzna:<br>';                    // Utworzenie komunikatu.
  msg += location.coords.longitude + '</h3>';               // Dodanie długości geograficznej.
  msg += '<h3>Szerokość geograficzna:<br>';                 // Utworzenie komunikatu.
  msg += location.coords.latitude + '</h3>';                // Dodanie szerokości geograficznej.
  elMap.innerHTML = msg;                                    // Wyświetlenie współrzędnych geograficznych.
}

function fail(msg) {                                        // Nie otrzymano danych o położeniu.
  elMap.textContent = msg;                                  // Wyświetlenie komunikatu.
  console.log(msg.code);                                    // Wyświetlenie komunikatu.
}