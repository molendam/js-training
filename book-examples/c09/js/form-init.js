$(function() {

  $('#arrival').datepicker();      // Zmiana pola tekstowego w kontrolkę JQUI datepicker.

  var $amount = $('#amount');      // Buforowanie danych wejściowych (cena).
  var $range = $('#price-range');  // Buforowanie <div> dla zakresu ceny.
  $('#price-range').slider({       // Zmiana pola zakresu ceny na suwak.
    range: true,                   // Jeżeli to zakres, konieczne są dwa uchwyty.
    min: 0,                        // Wartość minimalna.
    max: 400,                      // Wartość minimalna.
    values: [175, 300],            // Wartości używane podczas wczytywania strony. 
    slide: function(event, ui) {   // Po użyciu suwaka należy uaktualnić element amount.
      $amount.val(ui.values[0] + ' zł - ' + ui.values[1] + ' zł');
    }
  });
  $amount                          // Ustawienie wartości początkowych dla elementu amount.
    .val($range.slider('values', 0) + ' zł'     // Mniejsza wartość zakresu, a następnie "zł".
    + ' - ' + $range.slider('values', 1) + ' zł'); // Większa wartość zakresu, a następnie "zł".

});