function init() {
  var mapOptions = {                                 // Konfiguracja opcji mapy.
    center: new google.maps.LatLng(40.782710,-73.965310),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    scaleControl: true,
    zoom: 13
  };
  var venueMap;                                      // Metoda Map() powoduje wygenerowanie mapy.
  venueMap = new google.maps.Map(document.getElementById('map'), mapOptions);
}

function loadScript() {
  var script = document.createElement('script');     // Utworzenie elementu <script>.
  script.src = 'http://maps.googleapis.com/maps/api/js?sensor=false&callback=init';
  document.body.appendChild(script);                 // Umieszczenie elementu na stronie.
}

window.onload = loadScript;                          // Wywołanie zdarzenia onload.