$(function() {                                  // Model DOM został wczytany.
  function loadContent(url){                    // Wczytanie nowej zawartości na stronie.
    $('#content').load(url + ' #container').hide().fadeIn('slow');
  }

  $('nav a').on('click', function(e) {          // Procedura obsługi kliknięcia. 
    e.preventDefault();                         // Uniemożliwienie przejścia na nową stronę.
    var href = this.href;                       // Pobranie wartości atrybutu href łącza.
    var $this = $(this);                        // Przechowywanie łącza w obiekcie jQuery.
    $('a').removeClass('current');              // Usunięcie klasy current z łączy.
    $this.addClass('current');                  // Dodanie klasy current.
    loadContent(href);                          // Wywołanie funkcji: wczytanie zawartości.
    history.pushState('', $this.text, href);    // Uaktualnienie historii.
  });

  window.onpopstate = function() {              // Obsługa przycisków wstecz/dalej.
    var path = location.pathname;               // Pobranie ścieżki dostępu do pliku.
    loadContent(path);                          // Wywołanie funkcji w celu wczytania strony.
￼￼￼￼var page = path.substring(location.pathname.lastIndexOf("/") + 1);
    $('a').removeClass('current');              // Usunięcie klasy current z łączy.
    $('[href="' + page + '"]').addClass('current'); // Dodanie klasy current.
  };
});