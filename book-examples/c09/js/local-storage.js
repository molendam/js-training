if (Modernizr.localstorage) {

  var txtUsername = document.getElementById('username'); // Pobranie elementów formularza.
  var txtAnswer = document.getElementById('answer');

  txtUsername.value = localStorage.getItem('username');  // Wartości elementów są
  txtAnswer.value = localStorage.getItem('answer');      // pobrane z obiektu localStorage.

  txtUsername.addEventListener('input', function () {    // Dane zostały zapisane.
    localStorage.setItem('username', txtUsername.value);
  }, false);

  txtAnswer.addEventListener('input', function () {      // Dane zostały zapisane.
    localStorage.setItem('answer', txtAnswer.value);
  }, false);
}