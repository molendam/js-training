var $form, width, height, area;
$form = $('#calculator');

$('form input[type="text"]').on('blur', function() {
  // Ten komunikat zostanie wyświetlony tylko wtedy, gdy wartość wprowadzona przez użytkownika jest mniejsza niż 10.
  console.assert(this.value > 10, 'Użytkownik wprowadził wartość mniejszą niż 10');
});

$('#calculator').on('submit', function(e) { 
  e.preventDefault();
  console.log('Kliknąłeś przycisk...');

  width = $('#width').val();
  height = $('#height').val();
  area = width * height;
  // Ten komunikat zostanie wyświetlony tylko wtedy, gdy wartość wprowadzona przez użytkownika nie jest liczbowa.
  console.assert($.isNumeric(area), 'Użytkownik wprowadził wartość nieliczbową');

  $form.append('<p>' + area + '</p>');
});