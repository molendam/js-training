console.info('I oto jesteśmy...');                    // Info: skrypt został uruchomiony.

var $form, width, height, area;
$form = $('#calculator');

$('form input[type="text"]').on('blur', function() {  // Po wystąpieniu zdarzenia blur.
  console.warn('Wprowadziłeś ', this.value);          // Ostrzeżenie: wprowadzone dane.
});

$('#calculator').on('submit', function(e) {           // Po wysłaniu formularza sieciowego.
  e.preventDefault();

  width = $('#width').val();
  height = $('#height').val();

  area = width * height;
  console.error(area);                                // Błąd: wyświetlenie obliczonego pola.

  $form.append('<p class="result">' + area + '</p>');
});