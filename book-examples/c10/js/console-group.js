var $form = $('#calculator');

$form.on('submit', function(e) {                 // Kod wykonywany po wysłaniu formularza.
  e.preventDefault();
  console.log('Kliknąłeś przycisk...');          // Wyświetlenie informacji o klikniętym przycisku.

  var width, height, area;
  width = $('#width').val();
  height = $('#height').val();
  area = width * height;

  console.group('Obliczenie pola');              // Początek grupowania.
    console.info('Szerokość ', width);           // Wyświetlenie długości.
    console.info('Wysokość ', height);           // Wyświetlenie wysokości.
    console.log(area);                           // Wyświetlenie pola.
  console.groupEnd();                            // Koniec grupowania.

  $form.append('<p>' + area + '</p>');
});