console.log('I oto jesteśmy...');               // Wskazuje, że skrypt został uruchomiony.
var $form, width, height, area;
$form = $('#calculator');

$('form input[type="text"]').on('blur', function() { // Kiedy pole przestaje być aktywne.
  console.log('Wprowadziłeś ', this.value );         // Wyświetlenie wartości w konsoli.
});

$('#calculator').on('submit', function(e) {     // Kiedy użytkownik naciśnie przycisk wysyłający formularz.
  e.preventDefault();                           // Uniemożliwienie wysłania formularza.
  console.log('Kliknąłeś przycisk...');         // Wskazuje kliknięcie przycisku.

  width = $('#width').val();
  console.log('Długość ' + width);              // Wyświetlenie w konsoli wartości długości.

  height = $('#height').val();
  console.log('Wysokość ', height);             // Wyświetlenie w konsoli wartości wysokości.

  area = width * height;
  console.log(area);                            // Wyświetlenie w konsoli wartości pola.

  $form.append('<p>' + area + '</p>')
});