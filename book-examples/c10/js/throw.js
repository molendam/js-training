var width = 12;                                     // Zmienna width.
var height = 'test';                                // Zmienna height.

function calculateArea(width, height) {
  try {
    var area = width * height;                      // Próba obliczenia pola.
    if (!isNaN(area)) {                             // Jeżeli wynik jest liczbą,
      return area;                                  // zwracane jest obliczone pole.
    } else {                                        // W przeciwnym razie zgłaszamy błąd.
      throw new Error('Funkcja calculateArea() otrzymała nieprawidłowe dane');
    }
  } catch(e) {                                     // Jeżeli wystąpił błąd.
    console.log(e.name + ' ' + e.message);          // Wyświetlenie w konsoli komunikatu o błędzie.
    return 'Nie można obliczyć pola.'; // Wyświetlenie komunikatu o błędzie użytkownikowi.
  }
}

// Próba wyświetlenia na stronie obliczonego pola.
document.getElementById('area').innerHTML = calculateArea(width, height);