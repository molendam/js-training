var contacts = {                 // Informacje są przechowywane w postaci literału obiektu.
  "Londyn": {
    "Tel.": "+44 (0)207 946 0128",
    "Kraj": "UK"},
  "Sydney": {
    "Tel.": "+61 (0)2 7010 1212",
    "Kraj": "Australia"},
  "Nowy Jork": {
    "Tel.": "+1 (0)1 555 2104",
    "Kraj": "USA"}
};

console.table(contacts);                   // Wyświetlenie danych w konsoli.

var city, contactDetails;                  // Deklaracja zmiennych.
contactDetails = '';                       // Zmienna przeznaczona na dane wyświetlane na stronie.

$.each(contacts, function(city, contacts) {
  contactDetails += city + ': ' + contacts.Tel + '<br />';
});
$('h2').after('<p>' + contactDetails + '</p>');