var feed = document.getElementById('feed');

// Correct feed
var response = '{"deals": [{"title": "Sklep Farrow i Ball","description": "Najnowsze pojemniki 2.5l w znacznie niższych cenach, tylko w tym tygodniu","price": 30,"link": "http://www.example.com/farrow-and-ball/"},{"title": "Siecle Paints z Wielkiej Brytanii","description": "Prawdopodobnie najlepsze obrazy na świecie","price": 28,"link": "http://www.example.com/siecle/"},{"title": "Kelly Hoppen","description": "Wyposażenie wnętrz opracowane przez Kelly Hoppen","price": 42,"link": "http://www.example.com/kelly-hoppen/"}]}';
// Dane zawierające błąd - umieść poniższy wiersz w komentarzu, aby zobaczyć jak te dane zostaną obsłużone.
response = '{"deals": [{"title": "Farrow and Ball","description": "New season 2.5l '; // Dane JSON.

if (response) {
  try {
    var dealData = JSON.parse(response);              // Próba przetworzenia JSON.
    showContent(dealData);                            // Wyświetlenie danych JSON.
  } catch(e) {
    var errorMessage = e.name + ' ' + e.message;      // Utworzenie komunikatu o błędzie.
    console.log(errorMessage);                        // Wyświetlenie komunikatów w konsoli.
    feed.innerHTML = '<em>Przepraszamy, nie znaleziono promocji</em>';// Komunikat dla użytkowników.
  } finally {
    var link = document.createElement('a');           // Dodanie łącza odświeżającego dokument.
    link.innerHTML = ' <a href="try-catch-finally.html">odśwież</a>';
    feed.appendChild(link);
  }
}

function showContent(dealData) {
  var newContent = '';
  for (var i in dealData.deals) {
    newContent += '<div class="deal">';
    newContent += '<h2>' + dealData.deals[i].title + '</h2>';
    newContent += '<p>' + dealData.deals[i].description + '</p>';
    newContent += '<a href="' + dealData.deals[i].link + '">';
    newContent += ' ' + dealData.deals[i].link;
    newContent +='</a>';
    newContent += '</div>';
  }
  feed.innerHTML = newContent;
}