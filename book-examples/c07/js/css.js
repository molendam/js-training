$(function() {

  // Pobranie koloru tła pierwszego elementu listy.
  var backgroundColor = $('li').css('background-color');

  // Wyświetlenie pod listą nazwy tego koloru.
  $('ul').append('<p>Kolor to: ' + backgroundColor + '</p>');

  // Ustawienie kilku właściwości we wszystkich elementach listy.
  $('li').css({
    'background-color': '#c5a996',
    'border': '1px solid #fff',
    'color': '#000',
    'text-shadow': 'none',
    'font-family': 'Georgia',
    'padding-left': '+=75'
  });
});