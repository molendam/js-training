$(function() {
  var listItem, itemStatus, eventType;

  $('ul').on(
    'click mouseover',
    ':not(#four)',
    {status: 'important'},
    function(e) {
      listItem = 'Element: ' + e.target.textContent + '<br />';
      itemStatus = 'Stan: ' + e.data.status + '<br />';
      eventType = 'Zdarzenie: ' + e.type;
      $('#notes').html(listItem + itemStatus + eventType);
    }
  );

});