var elList, addLink, newEl, newText, counter, listItems; // Deklaracja zmiennych.

elList  = document.getElementById('list');               // Pobranie listy.
addLink = document.querySelector('a');                   // Pobranie przycisku dodawania elementu.
counter = document.getElementById('counter');            // Pobranie elementu licznika.

function addItem(e) {                                    // Deklaracja funkcji.
  e.preventDefault();                                    // Uniemożliwienie akcji łącza.
  newEl = document.createElement('li');                  // Nowy element <li>.
  newText = document.createTextNode('Nowy element listy'); // Nowy węzeł tekstowy.
  newEl.appendChild(newText);                            // Dodanie tekstu do elementu <li>.
  elList.appendChild(newEl);                             // Dodanie elementu <li> do listy.
}

function updateCount() {                                 // Deklaracja funkcji.
  listitems = list.getElementsByTagName('li').length;    // Ustalenie całkowitej liczby elementów <li>.
  counter.innerHTML = listitems;                         // Uaktualnienie licznika.
}

addLink.addEventListener('click', addItem, false);       // Kliknięcie przycisku.
elList.addEventListener('DOMNodeInserted', updateCount, false); // Model DOM uaktualniony.