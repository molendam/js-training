function setup() {                                  // Deklaracja funkcji.
  var textInput;                                    // Utworzenie zmiennej.
  textInput = document.getElementById('username');  // Pobranie pola tekstowego username.
  textInput.focus();                                // Uaktywnienie elementu pola tekstowego.
}

window.addEventListener('load', setup, false); // Po wczytaniu strony będzie wywołana funkcja setup().


/* Dłuższa wersja zapewniająca zgodność z IE8 (oraz starszymi wersjami IE).

if (el.addEventListener) {
      el.addEventListener('click', function(e) {
        itemDone(e);
    }, false);
} else {
    el.attachEvent('onload', function(e){
      itemDone(e);
    });
}

*/