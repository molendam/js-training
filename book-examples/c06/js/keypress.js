var el;                                                    // Deklaracja zmiennych.

function charCount(e) {                                    // Deklaracja funkcji.
  var textEntered, charDisplay, counter, lastkey;          // Deklaracja zmiennych.
  textEntered = document.getElementById('message').value;  // Tekst podany przez użytkownika.
  charDisplay = document.getElementById('charactersLeft'); // Element licznika.
  counter = (180 - (textEntered.length));                  // Liczba pozostałych znaków.
  charDisplay.textContent = counter;                       // Wyświetlenie liczby pozostałych znaków.

  lastkey = document.getElementById('lastKey');            // Sprawdzenie ostatniego naciśniętego klawisza.
  lastkey.textContent = 'Ostatni klawisz ma kod ASCII: ' + e.keyCode; // Utworzenie komunikatu. 
}
el = document.getElementById('message');                   // Utworzenie komunikatu. 
el.addEventListener('keypress', charCount, false); // Zdarzenie keypress - wywołanie funkcji charCount().