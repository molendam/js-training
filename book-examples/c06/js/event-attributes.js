function checkUsername() {                             // Deklaracja funkcji.
  var elMsg = document.getElementById('feedback');     // Pobranie elementu feedback.
  var elUsername = document.getElementById('username');// Pobranie nazwy użytkownika.
  if (elUsername.value.length < 5) {                   // Jeżeli nazwa użytkownika jest zbyt krótka.
    elMsg.textContent = 'Nazwa użytkownika musi mieć przynajmniej 5 znaków.'; // Komunikat.
  } else {                                              // W przeciwnym razie.
    elMsg.textContent = '';                             // Usunięcie komunikatu.
  }
}