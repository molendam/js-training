var elUsername = document.getElementById('username');   // Pobranie pola tekstowego username.
var elMsg      = document.getElementById('feedback');   // Pobranie elementu feedback. 

function checkUsername(minLength) {                     // Deklaracja funkcji.
  if (elUsername.value.length < minLength) {            // Jeżeli nazwa użytkownika jest zbyt krótka.
    // Przygotowanie komunikatu o błędzie.
    elMsg.innerHTML = 'Nazwa użytkownika musi mieć przynajmniej ' + minLength + ' znaków';
  } else {                                             // W przeciwnym razie.
    elMsg.innerHTML = '';                              // Usunięcie komunikatu.
  }
}

elUsername.addEventListener('blur', function() {        // Gdy element stanie się nieaktywny.
  checkUsername(5);                                     // W tym miejscu są przekazywane argumenty.
}, false);