function setup() {
  var textInput;
  textInput = document.getElementById('message');
  textInput.focus();
}

window.addEventListener('DOMContentLoaded', setup, false);

window.addEventListener('beforeunload', function(event){
  // Ten przykład został uaktualniony, aby zapewnić mu lepsze działanie w przeglądarkach internetowych (zgodnie z zaleceniem MDN).
  var message = 'Masz niezapisane zmiany...';
  (event || window.event).returnValue = message;
  return message;
});