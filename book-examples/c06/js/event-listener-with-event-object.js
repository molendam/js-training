function checkLength(e, minLength) {         // Deklaracja funkcji.
  var el, elMsg;                             // Deklaracja zmiennych.
  if (!e) {                                  // Jeżeli obiekt event nie istnieje.
    e = window.event;                        // Użycie rozwiązania awaryjnego dla IE.
  }
  el = e.target || e.srcElement;             // Pobranie elementu docelowego dla zdarzenia.
  elMsg = el.nextSibling;                    // Pobranie następnego elementu równorzędnego.

  if (el.value.length < minLength) {         // Jeżeli długość jest niewystarczająca, definiujemy komunikat.
    elMsg.innerHTML = 'Nazwa użytkownika musi mieć przynajmniej ' + minLength + ' znaków';
  } else {                                   // W przeciwnym razie.
    elMsg.innerHTML = '';                    // Usunięcie komunikatu.
  }
}

var elUsername = document.getElementById('username'); // Pobranie pola tekstowego username.
if (elUsername.addEventListener) {           // Jeżeli obserwator zdarzeń jest obsługiwany.
  elUsername.addEventListener('blur', function(e) {  // W przypadku zdarzenia blur.
    // Uwaga: to jest funkcja checkLength(), a nie checkUsername().
    checkLength(e, 5);                             // Wywołanie funkcji checkLength().
  }, false);                                       // Przechwycenie podczas fazy propagacji zdarzeń.
} else {                                           // W przeciwnym razie.
  elUsername.attachEvent('onblur', function(e){    // Rozwiązanie awaryjne dla IE: onblur.
    // Uwaga: to jest funkcja checkLength(), a nie checkUsername().
    checkLength(e, 5);                             // Wywołanie funkcji checkLength().
  });
}