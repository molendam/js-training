var elForm, elSelectPackage, elPackageHint, elTerms, elTermsHint; // Deklaracja zmiennych.
elForm          = document.getElementById('formSignup');          // Przechowywanie elementów.
elSelectPackage = document.getElementById('package');
elPackageHint   = document.getElementById('packageHint');
elTerms         = document.getElementById('terms');
elTermsHint     = document.getElementById('termsHint');

function packageHint() {                                 // Deklaracja funkcji.
  var pack = this.options[this.selectedIndex].value;     // Pobranie wybranej opcji.
  if (pack === 'monthly') {                              // Jeżeli wartością jest monthly.
    elPackageHint.innerHTML = 'Płacąc za rok oszczędzasz 10 zł!'; // Wyświetlenie tego komunikatu.
  } else {                                               // W przeciwnym razie.
    elPackageHint.innerHTML = 'Dobry wybór!';            // Wyświetlenie tego komunikatu.
  }
}

function checkTerms(event) {                             // Deklaracja funkcji.
  if (!elTerms.checked) {                                // Jeżeli zaznaczono pole wyboru.
    elTermsHint.innerHTML = 'Musisz się zgodzić z warunkami.'; // Wyświetlenie komunikatu.
    event.preventDefault();                              // Nie wysyłaj formularza sieciowego.
  }
}

// Utworzenie obserwatorów zdarzeń: submit wywołuje checkTerms(), natomiast change wywołuje packageHint().
elForm.addEventListener('submit', checkTerms, false);
elSelectPackage.addEventListener('change', packageHint, false);