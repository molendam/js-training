function getTarget(e) {                          // Deklaracja funkcji.
  if (!e) {                                      // Jeżeli obiekt event nie istnieje.
    e = window.event;                            // Użycie starego obiektu event w IE.
  }
  return e.target || e.srcElement;               // Pobranie elementu docelowego dla zdarzenia.
}

function itemDone(e) {                           // Deklaracja funkcji.
  // Usunięcie elementu z listy.
  var target, elParent, elGrandparent;           // Deklaracja zmiennych.
  target = getTarget(e);                         // Pobranie klikniętego elementu.
  elParent = target.parentNode;                  // Pobranie elementu nadrzędnego dla klikniętego.
  elGrandparent = target.parentNode.parentNode;  // Pobranie listy.
  elGrandparent.removeChild(elParent);           // Usunięcie elementu listy.

  // Uniemożliwienie zachowania domyślnego łącza, czyli przeniesienia użytkownika na inną stronę.
  if (e.preventDefault) {                        // Czy metoda preventDefault() jest obsługiwana?
    e.preventDefault();                          // Użycie metody preventDefault().
  } else {                                       // W przeciwnym razie.
    e.returnValue = false;                       // Użycie starszej wersji IE.
  }
}

// Konfiguracja obserwatora zdarzeń: wywołanie itemDone() w odpowiedzi na zdarzenie click.
var el = document.getElementById('shoppingList');// Pobranie listy rzeczy do kupienia.
if (el.addEventListener) {                       // Jeżeli obserwator zdarzeń jest obsługiwany.
  el.addEventListener('click', function(e) {     // Dodanie obserwatora.
    itemDone(e);                                 // Wywołanie itemDone().
  }, false);                                     // Użycie propagacji zdarzenia.
} else {                                         // W przeciwnym razie.
  el.attachEvent('onclick', function(e){         // Użycie starego modelu IE: onclick.
    itemDone(e);                                 // Wywołanie itemDone().
  });
}