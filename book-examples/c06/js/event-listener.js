function checkUsername() {                             // Deklaracja funkcji.
  var elMsg = document.getElementById('feedback');     // Pobranie elementu feedback.
  if (this.value.length < 5) {                         // Jeżeli nazwa użytkownika jest zbyt krótka.
    elMsg.textContent = 'Nazwa użytkownika musi mieć przynajmniej 5 znaków.'; // Komunikat.
  } else {                                             // W przeciwnym razie.
    elMsg.textContent = '';                            // Usunięcie komunikatu.
  }
}

var elUsername = document.getElementById('username');  // Pobranie pola tekstowego username.
// Gdy stanie się nieaktywne, należy wywołać funkcję checkUsername().
elUsername.addEventListener('blur', checkUsername, false);