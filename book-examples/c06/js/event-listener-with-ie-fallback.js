var elUsername = document.getElementById('username');  // Pobranie pola tekstowego username.
var elMsg = document.getElementById('feedback');       // Pobranie elementu feedback. 

function checkUsername(minLength) {                    // Deklaracja funkcji.
  if (elUsername.value.length < minLength) {           // Jeżeli nazwa użytkownika jest zbyt krótka.
    // Przygotowanie komunikatu.
    elMsg.innerHTML = 'Nazwa użytkownika musi mieć przynajmniej ' + minLength + ' znaków';
  } else {                                             // W przeciwnym razie.
    elMsg.innerHTML = '';                              // Usunięcie komunikatu.
  }
}

if (elUsername.addEventListener) {               // Jeżeli obserwator zdarzeń jest obsługiwany.
  elUsername.addEventListener('blur', function(){// Gdy pole staje się nieaktywne.
    checkUsername(5);                            // Wywołanie funkcji checkUsername().
  }, false);                                     // Przechwycenie podczas fazy propagacji zdarzeń.
} else {                                         // W przeciwnym razie.
  elUsername.attachEvent('onblur', function(){   // Rozwiązanie awaryjne dla IE: onblur.
    checkUsername(5);                            // Wywołanie funkcji checkUsername().
  });
}