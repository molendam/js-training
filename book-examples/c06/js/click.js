// Przygotowanie kodu HTML dla komunikatu.
var msg = '<div class=\"header\"><a id=\"close\" href="#">zamknij X</a></div>';
msg += '<div><h2>Konserwacja systemu</h2>';
msg += 'Między godzinami 3 a 4 nasze serwery będą konserwowane.  ';
msg += 'W tym czasie mogą wystąpić niewielkie zakłócenia w działaniu usług.</div>';

var elNote = document.createElement('div');       // Utworzenie nowego elementu.
elNote.setAttribute('id', 'note');                // Dodanie atrybutu id o wartości note.
elNote.innerHTML = msg;                           // Dodanie komunikatu.
document.body.appendChild(elNote);                // Umieszczenie elementu na stronie.

function dismissNote() {                          // Deklaracja funkcji.
  document.body.removeChild(elNote);              // Usunięcie elementu.
}

var elClose = document.getElementById('close');   // Pobranie przycisku zamykającego.
elClose.addEventListener('click', dismissNote, false); // Kliknięcie przycisku zamykającego.