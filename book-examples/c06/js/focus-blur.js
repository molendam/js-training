function checkUsername() {                        // Deklaracja funkcji.
  var username = el.value;                        // Nazwa użytkownika jest przechowywana w zmiennej.
  if (username.length < 5) {                      // Jeżeli nazwa użytkownika ma mniej niż 5 znaków.
    elMsg.className = 'warning';                  // Zmiana atrybutu class w elemencie komunikatu.
    elMsg.textContent = 'Nazwa nadal zbyt krótka...';// Uaktualnienie komunikatu.
  } else {                                        // W przeciwnym razie.
    elMsg.textContent = '';                       // Usunięcie komunikatu.
  }
}

function tipUsername() {                          // Deklaracja funkcji.
  elMsg.className = 'tip';                        // Zmiana atrybutu class w elemencie komunikatu.
  elMsg.innerHTML = 'Nazwa użytkownika musi mieć przynajmniej 5 znaków'; // Dodanie komunikatu.
}

var el = document.getElementById('username');     // Pobranie pola tekstowego username.
var elMsg = document.getElementById('feedback');  // Element wyświetlający komunikat.

// Kiedy pole tekstowe staje się aktywne lub nieaktywne, należy wywołać odpowiednią funkcję:
el.addEventListener('focus', tipUsername, false); // Aktywne: tipUsername().
el.addEventListener('blur', checkUsername, false);// Nieaktywne: checkUsername().

/* Dłuższa wersja zapewniająca zgodność z IE8 (oraz starszymi wersjami IE).

if (el.addEventListener) {
  el.addEventListener('focus', tipUsername, false);
  el.addEventListener('blur', checkUsername, false);
} else {
  el.attachEvent('onfocus', tipUsername);
  el.attachEvent('onblur', checkUsername);
}

*/