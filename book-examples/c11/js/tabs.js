$('.tab-list').each(function(){                   // Wyszukanie list kart.
  var $this = $(this),                            // Przechowywanie bieżącej listy.
      $tab = $this.find('li.active'),             // Pobranie aktywnego elementu listy.
      $link = $tab.find('a'),                     // Pobranie łącza z aktywnej karty.
      $panel = $($link.attr('href'));             // Pobranie aktywnego panelu.

  $this.on('click', '.tab-control', function(e) { // Po kliknięciu karty.
    e.preventDefault();                           // Uniemożliwienie domyślnego zachowania elementu.
    var $link = $(this),                          // Przechowywanie bieżącego łącza.
        id = this.hash;                           // Pobranie atrybutu href klikniętej karty. 

    if (id && !$link.is('.active')) {             // Jeżeli nie jest aktualnie aktywny.
      $panel.removeClass('active');               // Panel staje się nieaktywny.
      $tab.removeClass('active');                 // Karta staje się nieaktywna.

      $panel = $(id).addClass('active');          // Panel staje się aktywny.
      $tab = $link.parent().addClass('active');   // Karta staje się aktywna.
    }
  });
});