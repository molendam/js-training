(function($) {                                    // Użycie $ jako nazwy zmiennej.
  $.fn.accordion = function (speed) {             // Zwrot elementów wybranych w jQuery.
    this.on('click', '.accordion-control', function (e) {
      e.preventDefault();
      $(this)
        .next('.accordion-panel')
        .not(':animated')
        .slideToggle(speed);
    });
    return this;                                 // Zwrot elementów wybranych w jQuery.
  };
}(jQuery));                                      // Przekazanie w obiekcie jQuery.