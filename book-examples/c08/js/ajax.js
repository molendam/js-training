$('nav a').on('click', function(e) {
  e.preventDefault();
  var url = this.href,                                      // Adres URL strony do wczytania.
      $content = $('#content');                             // Buforowanie wyboru.

  $('nav a.current').removeClass('current');                // Uaktualnienie łączy.
  $(this).addClass('current');
  $('#container').remove();                                 // Usunięcie zawartości.

  $.ajax({
    type: "POST",                                           // Wybór metody: GET lub POST.
    url: url,                                               // Ścieżka dostępu do pliku.
    timeout: 2000,                                          // Czas oczekiwania.
    beforeSend: function() {                                // Przed wykonaniem żądania Ajax. 
      $content.append('<div id="load">Wczytywanie</div>');  // Wczytanie komunikatu.
    },
    complete: function() {                                  // Po wykonaniu żądania Ajax.
      $('#loading').remove();                               // Usunięcie komunikatu.
    },
    success: function(data) {                               // Wyświetlenie zawartości.
      $content.html( $(data).find('#container') ).hide().fadeIn(400);
    },
    fail: function() {                                      // Wyświetlenie komunikatu o błędzie. 
      $('#panel').html('<div class="loading">Proszę spróbować wkrótce.</div>');
    }
  });

});