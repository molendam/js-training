$('#register').on('submit', function(e) {           // Po wysłaniu formularza sieciowego.
  e.preventDefault();                               // Uniemożliwienie wysłania formularza.
  var details = $('#register').serialize();         // Serializacja danych formularza.
  $.post('register.php', details, function(data) {  // Użycie metody $.post() do wysłania danych.
    $('#register').html(data);                      // Miejsce wyświetlenia wyniku.
  });
});