function showEvents(data) {                           // Funkcja wywołania zwrotnego po wczytaniu JSON.
  var newContent = '';                                // Zmienna przechowująca zawartość HTML.
 
    // Utworzenie ciągu tekstowego wraz z nową zawartością (można użyć także operacji opartych na modelu DOM).
    for (var i = 0; i < data.events.length; i++) {    // Iteracja przez dane.
      newContent += '<div class="event">';
      newContent += '<img src="' + data.events[i].map + '" ';
      newContent += 'alt="' + data.events[i].location + '" />';
      newContent += '<p><b>' + data.events[i].location + '</b><br>';
      newContent += data.events[i].date + '</p>';
      newContent += '</div>';
    }

    // Uaktualnienie strony nową zawartością.
    document.getElementById('content').innerHTML = newContent;
}