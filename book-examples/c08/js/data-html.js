var xhr = new XMLHttpRequest();                 // Utworzenie obiektu XMLHttpRequest.

xhr.onload = function() {                       // Po wczytaniu odpowiedzi.
  // Poniższa konstrukcja warunkowa nie działa lokalnie; działa jedynie w serwerze.
  if(xhr.status === 200) {                       // Jeżeli stan serwera wskazuje, że wszystko jest w porządku.
    document.getElementById('content').innerHTML = xhr.responseText; // Uaktualnienie.
  }
};

xhr.open('GET', 'data/data.html', true);        // Przygotowanie żądania.
xhr.send(null);                                 // Wykonanie żądania.

// Uwaga: jeżeli uruchamiasz ten plik lokalnie,
// wtedy nie otrzymasz informacji o stanie serwera.
// Istnieje możliwość przypisania wartości true w poleceniu warunkowym (wiersz 5.), jak przedstawiono poniżej:
// if(true) {