$('nav a').on('click', function(e) {                 // Użytkownik kliknął łącze.
  e.preventDefault();                                // Zatrzymanie wczytywania nowego łącza.
  var url = this.href;                               // Pobranie wartości atrybutu href.

  $('nav a.current').removeClass('current');         // Usunięcie klasy current.
  $(this).addClass('current');                       // Określenie nowego elementu jako bieżącego.

  $('#container').remove();                          // Usunięcie starej zawartości.
  $('#content').load(url + ' #content').hide().fadeIn('slow'); // Nowa zawartość.
});