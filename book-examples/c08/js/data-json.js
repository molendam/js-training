var xhr = new XMLHttpRequest();                 // Utworzenie obiektu XMLHttpRequest.

xhr.onload = function() {                       // Po zmianie stanu.
  // Poniższa konstrukcja warunkowa nie działa lokalnie; działa jedynie w serwerze.
  if(xhr.status === 200) {                      // Jeżeli stan serwera wskazuje, że wszystko jest w porządku.
    responseObject = JSON.parse(xhr.responseText);

    // Utworzenie ciągu tekstowego wraz z nową zawartością (można użyć także operacji opartych na modelu DOM).
    var newContent = '';
    for (var i = 0; i < responseObject.events.length; i++) { // Iteracja przez obiekt.
      newContent += '<div class="event">';
      newContent += '<img src="' + responseObject.events[i].map + '" ';
      newContent += 'alt="' + responseObject.events[i].location + '" />';
      newContent += '<p><b>' + responseObject.events[i].location + '</b><br>';
      newContent += responseObject.events[i].date + '</p>';
      newContent += '</div>';
    }

    // Uaktualnienie strony nową zawartością.
    document.getElementById('content').innerHTML = newContent;

  }
};

xhr.open('GET', 'data/data.json', true);        // Przygotowanie żądania.
xhr.send(null);                                 // Wykonanie żądania.

// Uwaga: jeżeli uruchamiasz ten plik lokalnie,
// wtedy nie otrzymasz informacji o stanie serwera.
// Istnieje możliwość przypisania wartości true w poleceniu warunkowym (wiersz 5.), jak przedstawiono poniżej:
// if(true) {