$('#exchangerates').append('<div id="rates"></div><div id="reload"></div>');

function loadRates() {
  $.getJSON('data/rates.json')
  .done( function(data){                                 // Serwer zwraca dane.
    var d = new Date();                                  // Utworzenie obiektu daty.
    var hrs = d.getHours();                              // Określenie godziny.
    var mins = d.getMinutes();                           // Określenie minuty.
    var msg = '<h2>Kursy wymiany walut</h2>';            // Początek komunikatu.
    $.each(data, function(key, val) {                    // Dodanie poszczególnych kursów wymiany.
      msg += '<div class="' + key + '">' + key + ': ' + val + '</div>';
    });
    msg += '<br>Ostatnia aktualizacja: ' + hrs + ':' + mins + '<br>'; // Wyświetlenie uaktualnionej godziny.
    $('#rates').html(msg);                               // Umieszczenie na stronie kursów wymiany.
  }).fail( function() {                                  // Wystąpił błąd.
    $('aside').append('Przepraszamy, nie można pobrać danych.');   // Wyświetlenie komunikatu o błędzie.  
  }).always( function() {                                // Ta metoda zawsze jest wywoływana.
     var reload = '<a id="refresh" href="#">';           // Dodanie łącza odświeżającego zawartość.
     reload += '<img src="img/refresh.png" alt="odśwież" /></a>';
     $('#reload').html(reload);                          // Dodanie łącza odświeżającego zawartość.
     $('#refresh').on('click', function(e) {             // Dodanie procedury obsługi zdarzeń click.
       e.preventDefault();                               // Uniemożliwienie przejścia na nową stronę.
       loadRates();                                      // Wywołanie funkcji loadRates().
     });
  }); 
}

loadRates();                                             // Wywołanie funkcji loadRates().