var xhr = new XMLHttpRequest();        // Utworzenie obiektu XMLHttpRequest.

xhr.onload = function() {              // Po wczytaniu odpowiedzi.
 // Poniższa konstrukcja warunkowa nie działa lokalnie; działa jedynie w serwerze.
 if (xhr.status === 200) {             // Jeżeli stan serwera wskazuje, że wszystko jest w porządku.

// Ten fragment jest inny, ponieważ przetwarzane są dane XML, a nie HTML.
var response = xhr.responseXML;                      // Pobranie danych XML z serwera.
var events = response.getElementsByTagName('event'); // Wyszukanie elementów <event>.

for (var i = 0; i < events.length; i++) {            // Iteracja przez znalezione elementy.
 var container, image, location, city, newline;      // Deklaracja zmiennych.
 container = document.createElement('div');          // Utworzenie pojemnika <div>.
 container.className = 'event';                      // Dodanie atrybutu class.

 image = document.createElement('img');              // Dodanie obrazu mapy.
 image.setAttribute('src', getNodeValue(events[i], 'map'));
 image.appendChild(document.createTextNode(getNodeValue(events[i], 'map')));
 container.appendChild(image);

 location = document.createElement('p');             // Dodanie danych dotyczących lokalizacji.
 city = document.createElement('b');
 newline = document.createElement('br');
 city.appendChild(document.createTextNode(getNodeValue(events[i], 'location')));
 location.appendChild(newline);
 location.insertBefore(city, newline);
 location.appendChild(document.createTextNode(getNodeValue(events[i], 'date')));
 container.appendChild(location);

 document.getElementById('content').appendChild(container);
}
function getNodeValue(obj, tag) {                   // Pobranie zawartości z danych XML.
 return obj.getElementsByTagName(tag)[0].firstChild.nodeValue;
}

 // Ostatni fragment jest taki sam jak w przypadku danych HTML, ale żądanie dotyczy pliku XML.
 }
};

xhr.open('GET', 'data/data.xml', true);             // Przygotowanie żądania.
xhr.send(null);                                     // Wykonanie żądania.

// Uwaga: jeżeli uruchamiasz ten plik lokalnie,
// wtedy nie otrzymasz informacji o stanie serwera.
// Istnieje możliwość przypisania wartości true w poleceniu warunkowym (wiersz 5.), jak przedstawiono poniżej:
// if(true) {