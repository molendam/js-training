// Utworzenie zmiennej o nazwie msg do przechowywania komunikatu, który będzie wyświetlony na stronie.
// Ustalenie szerokości okna przeglądarki internetowej i umieszczenie tej wartości w zmiennej msg.
var msg = '<h2>okno przeglądarki</h2><p>szerokość: ' + window.innerWidth + '</p>';
// Ustalenie wysokości okna przeglądarki internetowej i dodanie tej wartości w zmiennej msg.
msg += '<p>wysokość: ' + window.innerHeight + '</p>';
// Ustalenie liczby elementów w historii przeglądarki internetowej i dodanie tej wartości w zmiennej msg.
msg += '<h2>historia</h2><p>elementy: ' + window.history.length + '</p>';
// Ustalenie szerokości ekranu komputera i dodanie tej wartości w zmiennej msg.
msg += '<h2>ekran</h2><p>szerokość: ' + window.screen.width + '</p>';
// Ustalenie wysokości ekranu komputera i dodanie tej wartości w zmiennej msg.
msg += '<p>wysokość: ' + window.screen.height + '</p>';

// Utworzenie zmiennej o nazwie el do przechowywania elementu, którego atrybut id ma wartość info.
var el = document.getElementById('info');
// Umieszczenie komunikatu w znalezionym elemencie.
el.innerHTML = msg;
// Określenie położenia bieżącej strony i jego wyświetlenie w oknie komunikatu.
alert('Bieżąca strona: ' + window.location);