// Konfiguracja obiektu.
var hotel = new Object();

hotel.name = 'Park';
hotel.rooms = 120;
hotel.booked = 77;
hotel.checkAvailability = function() {
    return this.rooms - this.booked;  
};

var elName = document.getElementById('hotelName'); // Pobranie elementu.
elName.textContent = hotel.name;                   // Uaktualnienie kodu HTML właściwością obiektu.

var elRooms = document.getElementById('rooms');    // Pobranie elementu.
elRooms.textContent = hotel.checkAvailability();   // Uaktualnienie kodu HTML wynikiem działania metody.