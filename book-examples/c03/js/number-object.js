// Utworzenie zmiennej do przechowywania liczby, która zostanie użyta.
var originalNumber = 10.23456;
// Utworzenie zmiennej do przechowywania komunikatu, który będzie wyświetlony na stronie.
var msg = '<h2>liczba początkowa</h2><p>' + originalNumber + '</p>';
// Zaokrąglenie liczby do trzech miejsc po przecinku i dodanie jej do zmiennej msg.
msg += '<h2>trzy miejsca dziesiętne</h2><p>' + originalNumber.toFixed(3); + '</p>';
// Zaokrąglenie liczby do dokładnie trzech miejsc po przecinku i dodanie jej do zmiennej msg.
msg += '<h2>trzy cyfry</h2><p>' + originalNumber.toPrecision(3) + '</p>';

// Utworzenie zmiennej o nazwie el do przechowywania elementu, którego atrybut id ma wartość info.
var el = document.getElementById('info');
// Umieszczenie komunikatu w znalezionym elemencie.
el.innerHTML = msg;