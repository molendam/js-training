// Konfiguracja obiektu.
var hotel = {
  name : 'Park',
  rooms : 120,
  booked : 77,
};

hotel.gym = true;
hotel.pool = false;
delete hotel.booked;

// Uaktualnienie kodu HTML.
var elName = document.getElementById('hotelName'); // Pobranie elementu.
elName.textContent = hotel.name;                   // Uaktualnienie kodu HTML właściwością obiektu.

var elPool = document.getElementById('pool');      // Pobranie elementu.
elPool.className = hotel.pool;                     // Uaktualnienie kodu HTML właściwością obiektu.

var elGym = document.getElementById('gym');        // Pobranie elementu.
elGym.className = hotel.gym;                       // Uaktualnienie kodu HTML właściwością obiektu.