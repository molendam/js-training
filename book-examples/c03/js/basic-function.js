// Utworzenie zmiennej o nazwie msg przeznaczonej do przechowywania nowego komunikatu.
var msg = 'Zapisz się do naszego newslettera, a otrzymasz 10% rabatu!';

// Utworzenie funkcji do uaktualnienia zawartości elementu, którego atrybut id ma wartość message.
function updateMessage() {
  var el = document.getElementById('message');
  el.textContent = msg;
}

// Wywołanie funkcji.
updateMessage();