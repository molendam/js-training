// Utworzenie zmiennej o nazwie msg do przechowywania komunikatu, który będzie wyświetlony na stronie.
// Ustalenie tytułu dokumentu i umieszczenie go w zmiennej msg.
var msg = '<p><b>tytuł strony: </b>' + document.title + '<br />';
// Ustalenie adresu URL dokumentu i dodanie go do zmiennej msg.
msg += '<b>adres strony: </b>' + document.URL + '<br />';
// Ustalenie daty ostatniej modyfikacji dokumentu i dodanie jej do zmiennej msg.
msg += '<b>ostatnia modyfikacja: </b>' + document.lastModified + '</p>';

// Utworzenie zmiennej o nazwie el do przechowywania elementu, którego atrybut id ma wartość footer.
var el = document.getElementById('footer');
// Umieszczenie komunikatu w znalezionym elemencie.
el.innerHTML = msg;