// Utworzenie zmiennej do przechowywania losowej liczby z zakrsu od 1 do 10.
var randomNum = Math.floor((Math.random() * 10) + 1);

// Utworzenie zmiennej o nazwie el do przechowywania elementu, którego atrybut id ma wartość info.
var el = document.getElementById('info');
// Umieszczenie liczby w znalezionym elemencie.
el.innerHTML = '<h2>losowo wygenerowana liczba</h2><p>' + randomNum + '</p>';