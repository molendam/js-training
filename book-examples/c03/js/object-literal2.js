// Konfiguracja obiektu.
var hotel = {
  name : 'Park',
  rooms : 120,
  booked : 77,
  checkAvailability : function() {
    return this.rooms - this.booked; // Słowo kluczowe "this" jest niezbędne, ponieważ wywołanie znajduje się wewnątrz funkcji.
  }
};

// Uaktualnienie kodu HTML.
var elName = document.getElementById('hotelName'); // Pobranie elementu.
elName.textContent = hotel.name;                   // Uaktualnienie kodu HTML właściwością obiektu.

var elRooms = document.getElementById('rooms');    // Pobranie elementu.
elRooms.textContent = hotel.checkAvailability();   // Uaktualnienie kodu HTML właściwością obiektu.