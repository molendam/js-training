//https://edabit.com/challenge/rvsvGvqZ3BzNieKqA

(function detectWord_myFirstSolution(str = "Hej") {
    var lowCaseMin = 97;
    var lowCaseMax = 122;
    var result = '';

    for (const l of str){
        var caseIndex = l.charCodeAt(0);
        if(caseIndex>=lowCaseMin && caseIndex<=lowCaseMax){
            result+=l;
        }
    }
}())

function detectWordTwo_theBestSolution(str) {
	return str.match(/[a-z]/g).join('');
}

